LED Light Controller
====================

The LED Light Controller is a simple board to control light strips.
These have hundreds of LEDs (60-120 led/meter) and use 12V (and ~0.5A/m).
This board offers 2x5 outputs, so to be able to control 10 light strips independently (1 per color, RGB requires 3 channels).

The brightness of the light strips is PWM-controlled (this works well with LEDs).
Different "modes" can be saved so to be able to rapidly select between user-defined settings.
You can control the lights over UART, or with (almost) an infrared remote control using NEC code (which you will have to configure using the UART port first).

The power for the light strips and the board is provided by an off-the-shelf PC power supply (compliant to the ATX specification).
These offer ~10A for the 12V output through the main large connector (with 20 pins for ATX v1.x and 24 pins for ATX v2.x), which is used for the 5 first output channels.
An additional 12V output is provided through a smaller 4-pin connector by ATX v2.x power supplies, used for the 5 last output channels.
**The second connector has 4 pins, with 2 yellow cables on the side with the clip, and 2 black cables on the other side. DON'T CONNECT the one with 4 different colors.**
This is just an extension for the 20-pin connector from ATX v1.x to become the 24-pin connector from ATX v2.x.

Each channel supports up to ~8A, but the total of each 5-channel group is limited by the current provided by the ATX.
You can check on the side of your power supply to know the limit of the 12V outputs.

The board has the same size as the side of an ATX power supply, so to be fixed on it.
Don't directly attach it on the case, as this will create short circuits.
Use the mounting hole to put stand-off screws, which you have to drill yourself on the power supply.

The board has a power resistor so to put a heavy load on the 5V output.
This increases the performance of your ATX power supply (which is a switch mode power supply), so to have better 12V outputs for the light strips.
A fan (standard 80mm 3-wire PC fan), which you have to connect to the board, will take care of dissipating the generated heat.

versioning
----------

the version is not defined is the files themselves, but are generated.

for the pcb:
- use `rake` to generate the outputs
- is will use the schematic `led-controller.sch`, produced a `led-controller_vX.YYY.sch`, and put the version inside
- is will use the layout `led-controller.pcb`, produced a `led-controller_vX.ZZZ.pcb`, and put the version inside
- `X` is the version of the board, defined in `version`
- version changes are described in `CHANGES.txt`
- version with letter (A,B,C,…) are releases, version with number (0,1,2,…) are intermediate working stages
- `YYY` and `ZZZ` are revisions, independent of the version and each other. they described how often the schematic/layout has been modified (git commits)
