Element["" "vitrohm serie KH 216-8" "" "KH216-8" 0 0 0 0 0 100 "hidename"]
(
  ElementLine [6mm  -4.5mm 56mm -4.5mm 0.5mm]
  ElementLine [56mm  -4.5mm 56mm 4.5mm 0.5mm]
  ElementLine [56mm  4.5mm 6mm 4.5mm 0.5mm]
  ElementLine [6mm  4.5mm 6mm -4.5mm 0.5mm]
	Pin[0mm 0mm 3mm 0.5mm 3.5mm 1mm "" "1" ""]
	Pin[62mm 0mm 3mm 0.5mm 3.5mm 1mm "" "2" ""]
)
