/* mode settings */
#define MODES 3 /* the number of modes */
extern uint8_t power; /* power state */
extern uint8_t mode; /* the current mode */
extern uint8_t brightness[MODES][CHANNELS_1+CHANNELS_2]; /* the mode brightness settings for the channels */

/* IR settings actions from the infrared remote control */
enum IR_ACTIONS { 
	POWER = 0,
	MODE,
	BRIGHTNESS_UP,
	BRIGHTNESS_DOWN,
	CHANNEL_NEXT,
	CHANNEL_PREVIOUS,
	IR_ACTION_END
};
extern uint8_t ir_keys[IR_ACTION_END][2]; /* the IR NEC values (address+command) for the actions */

/* verify integrity of the settings */
bool verify_settings(void);
/* create settings */
void initialize_settings(void);
/* save current settings into EEPROM */
void save_settings(void);
/* load settings from EEPROM */
void load_settings(void);
/* invalidate settings in EEPROM */
void reset_settings(void);
