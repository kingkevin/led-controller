/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This is part of the LED light controller program.
 * It handles the IR NEC protocol decoding.
 * More information at http://www.sbprojects.com/knowledge/ir/nec.php
 */

extern const uint16_t MARKS[2]; /* mark duration in µs [start,bit] */
extern const uint16_t SPACES[4]; /* space duration in µs [start,repeat,1,0] */

/* a decoded NEC IR command */
struct nec {
	bool valid;
	bool repeat;
	uint8_t address;
	uint8_t command;
};

/* convert the pulse from µs into NEC mark and spaces */
void time2nec(uint16_t* burst, uint8_t pulses);
/* decode the NEC mark and space into IR command data */
struct nec nec2data(uint16_t* burst, uint8_t pulses);
