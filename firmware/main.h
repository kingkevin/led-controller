/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This is the main part of the LED light controller program.
 * It handles all peripherals (power, fan, channels, IR, serial)
 */

/* peripheral I/O */
#define LED PD6 /* status LED */
#define IR PB0 /* IR receiver */
#define FAN PC5 /* FAN output */
#define PWR_OK PB1 /* ATX power OK */
#define nPS_ON PB2 /* ATX power supply ON switch */
#define T_OFFSET 320 /* measured temperature offset (see AVR122) */
#define T_FACTOR 0.653 /* measured temperature factor (see AVR122) */

/* channel definitions */
//#define CHANNELS_1 5 /* the number of outputs (zo use) for channel 1 (up to 5) */
#define CHANNELS_1 3 /* the number of outputs (zo use) for channel 1 (up to 5) */
//#define CHANNELS_2 5 /* the number of outputs (to use) for channel 2 (up tp 5) */
#define CHANNELS_2 3 /* the number of outputs (to use) for channel 2 (up tp 5) */
extern volatile uint8_t* PORTS[CHANNELS_1+CHANNELS_2]; /* channel ports */
extern volatile uint8_t* DDRS[CHANNELS_1+CHANNELS_2]; /* channel I/O configuration registers */
extern const uint8_t BITS[CHANNELS_1+CHANNELS_2]; /* channel bits */

/* initialize I/O, timers, … */
void ioinit(void);
/* display the help */
void help(void);
/* process user command coming from UART */
void uart_action(char* str);
/* process user command coming from IR */
void ir_action(uint8_t address, uint8_t command);
