#include <stdint.h> /* Standard Integer Types */
#include <stdlib.h> /* General utilities */
#include <stdbool.h> /* Boolean */
#include <avr/eeprom.h> /* EEPROM handling */

#include "main.h"
#include "settings.h"

/* initialize variables */
uint8_t power;
uint8_t mode;
uint8_t brightness[MODES][CHANNELS_1+CHANNELS_2];
uint8_t ir_keys[IR_ACTION_END][2];

const uint8_t MAGIC = 0x42; /* magic header */

bool verify_settings(void)
{
	bool to_return;
	uint8_t checksum = 0;
	uint8_t byte;
	uint16_t settings_size = sizeof(MAGIC)+sizeof(power)+sizeof(mode)+sizeof(brightness)+sizeof(ir_keys)+1; /* the bytes used for the checksum (magic header and checksum included) */
	for (uint16_t i=0; i<settings_size; i++) { /* calculate checksum */
		byte = eeprom_read_byte((const uint8_t*)i);
		if (0==i && byte!=MAGIC) { /* verify magic header first */
			return false;
		}
		checksum ^= byte;
	}
	/* verify checkecum */
	if (0==checksum) {
		to_return = true;
	} else {
		to_return = false;
	}
	return to_return;
}

void initialize_settings(void)
{
	power = 0; /* power is off */
	mode = 0; /* first mode used */
	for (uint8_t i=0; i<MODES; i++) { /* channel brightness set to 0 */
		for (uint8_t j=0; j<CHANNELS_1+CHANNELS_2; j++) {
			brightness[i][j] = 0;
		}
	}
	for (uint8_t i=0; i<IR_ACTION_END; i++) { /* codes for IR actions set to 0 */
		ir_keys[i][0] = 0;
		ir_keys[i][1] = 0;
	}
}

void save_settings(void)
{
	uint16_t addr = 0; /* address in the EEPROM */
	uint8_t checksum = 0; /* checksum of settings (including the magic header, else all 0 settings are calculated as valid) */
	eeprom_update_byte((uint8_t*)addr,MAGIC);
	checksum ^= MAGIC;
	addr++;
	eeprom_update_byte((uint8_t*)addr,power);
	checksum ^= power;
	addr++;
	eeprom_update_byte((uint8_t*)addr,mode);
	checksum ^= mode;
	addr++;
	for (uint8_t i=0; i<MODES; i++) {
		for (uint8_t j=0; j<CHANNELS_1+CHANNELS_2; j++) {
			eeprom_update_byte((uint8_t*)addr,brightness[i][j]);
			checksum ^= brightness[i][j];
			addr++;
		}
	}
	for (uint8_t i=0; i<IR_ACTION_END; i++) {
		eeprom_update_byte((uint8_t*)addr,ir_keys[i][0]);
		checksum ^= ir_keys[i][0];
		addr++;
		eeprom_update_byte((uint8_t*)addr,ir_keys[i][1]);
		checksum ^= ir_keys[i][1];
		addr++;
	}
	eeprom_update_byte((uint8_t*)addr,checksum);
}

void load_settings(void)
{
	uint16_t addr = 1; /* the address in the EEPROM (skip magic header) */
	power = eeprom_read_byte((uint8_t*)addr);
	addr++;
	mode = eeprom_read_byte((uint8_t*)addr);
	addr++;
	for (uint8_t i=0; i<MODES; i++) {
		for (uint8_t j=0; j<CHANNELS_1+CHANNELS_2; j++) {
			brightness[i][j] = eeprom_read_byte((uint8_t*)addr);
			addr++;
		}
	}
	for (uint8_t i=0; i<IR_ACTION_END; i++) {
		ir_keys[i][0] = eeprom_read_byte((uint8_t*)addr);
		addr++;
		ir_keys[i][1] = eeprom_read_byte((uint8_t*)addr);
		addr++;
	}
}

void reset_settings(void)
{
	eeprom_update_byte((uint8_t*)0,0); /* invalidate magic header so the verification fails */
}
